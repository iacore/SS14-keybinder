import yaml

keybinds = yaml.load(open('keybinds.yml'), yaml.Loader)['binds']
# print(keybinds)

keymap = {
# movement+action
"W": "Period",
"A": "O",
"S": "E",
"D": "U",
"Q": "Comma",
"E": "P",
"Z": "Q",
"X": "J",
"C": "K",

# chat
#"T": "",
"Period": "L", #local
#"Apostrophe": "",
"Comma": "R", #whisper
#"SemiColon": "",
#"LBracket": "",
#"RBracket": "",
#"Backslash": "",
"Slash": "C", # console

# editor
#"P": "",

#"H": "",
#"G": "",
#"I": "",
#"B": "",
#"Tilde": "",
#"V": "",
}

print("""\
version: 1
binds:""")

for action in keybinds:
    key, what = action['key'], action['function']
    if key.startswith('Mouse'): continue
    if key.startswith('Num'): continue
    if what.startswith('Text'): continue
    if what.startswith('Editor'): continue
    if what.startswith('Arcade'): continue
    if key[0] == 'F' and len(key) > 1:
        int(key[1:])#validate
        continue
    if key in ['Escape', 'Tab', 'Space', 'PageDown', 'PageUp', 'Up', 'Down', 'Left', 'Right', 'Return', 'Delete', 'Home', 'End', 'Backspace', 'Shift']: continue
    if key in keymap:
        key = keymap[key]
    print(f"""\
- key: {key}
  function: {what}\
""")

print("""\
leaveEmpty: []
...
""")
